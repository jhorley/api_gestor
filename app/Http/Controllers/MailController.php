<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ActivarCuenta;
use App\Models\Usuario;
use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{
    public function sendConfirmation($user_id)
    {
        try 
        {
            $user=Usuario::find($user_id);
            if(!$user->email_verified_at){
                Mail::to($user->email)->send(new ActivarCuenta($user,url("/api/usuarios/confirmar/".$user_id)));
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Correo enviado, revisa tu bandeja de entrada o en Spam');
            }else{
                $data=array(
                    'status'=> 'warning',
                    'code'=> 201,
                    'message'=> 'Tu correo ya ha sido verificado anteriormente');
            }
        } catch (\Throwable $error) {
            throw $error;
            $data=array(
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Te pedimos disculpas, no logramos enviar el correo. Vuelve a intentar más tarde');
        }
        return response()->json($data,200);
    }
}
