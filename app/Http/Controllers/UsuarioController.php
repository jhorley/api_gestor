<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Usuario;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Exports\UsuariosExport;
use Illuminate\Support\Facades\Storage;


class UsuarioController extends Controller
{
    public function login(Request $request){
        $jwtAuth= new JwtAuth();
        $email=$request['email'];
        $password=$request['password'];
        $getToken=(isset($request['gettoken']) && $request['gettoken']==true)? $request['gettoken'] : null;
        //  cifrar password 
        $pwd= \hash('sha256',$password);
        if(!is_null($password) && !is_null($email) && ($getToken==null || $getToken=='false')){
            $usuario=Usuario::where(['email'=>$email])->first();
            if(!isset($usuario->email)){
                $data=array(
                    'status'=> 'error',
                    'code'=> 404,
                    'message'=> 'Email no existe'
                );
            }else{
                if($usuario->user_estado=='INACTIVO'){
                    $data=array(
                        'status'=> 'warning',
                        'code'=> 100,
                        'message'=> 'Usuario Eliminado'
                    );
                }else if(!$usuario->email_verified_at){
                    $data=array(
                        'status'=> 'warning',
                        'code'=> 101,
                        'message'=> 'Usuario debe verificar email, revisa tu bandeja de entrada o en spam'
                    );
                }else{
                    $signup=$jwtAuth->signup($email,$pwd);
                    return \response()->json($signup,200);
                }
            }
        }elseif($getToken!=null){
            $signup=$jwtAuth->signup($email,$pwd,true);
            return \response()->json($signup,200);
        }else{
            $data=array(
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Envía tus datos por post'
            );
        }
        return response()->json($data,200);
    }

    public function getUsuario(Request $request){
        $jwtAuth= new JwtAuth();
        $user_id=$jwtAuth->getUserId($request->header('Authorization'));
        try {
            $usuario=Usuario::find($user_id);
            $nombre=$usuario->nombre;
            $data=[
                'usuario'=>$usuario
            ];
            return \response()->json(array(
                'data'=>$data,
                'code'=>200,
            ),200);
        } catch (\Throwable $th) {
            return \response()->json(array(
                'status'=>'error',
                'code'=>400,
                'message'=>'Token no válido. intenta iniciar sesión nuevamente'
            ),200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $usuarios=Usuario::where('user_estado','ACTIVO')->orderBy('nombre','asc')->get();
            $data=array(
                'data'=>$usuarios,
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Datos obtenidos'
            );
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Datos no encontrados'
            );
        }
        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $new_usuario=json_decode($request['usuario'],true);
            $usuario=new Usuario($new_usuario);
            $pwd=hash('sha256',$new_usuario['password']);
            $usuario->password=$pwd;
            if($usuario->save()){
                // si viene una foto, guardamos en local, y se le asigna la url de la imagen
                if(is_file($request['foto'])){
                    $path_foto=Storage::disk('local')->put('perfiles/'.$usuario->user_id, $request['foto']);
                    $usuario->foto_perfil=env('APP_URL_IMGS').$path_foto;
                    $usuario->save();
                }
                $data=array(
                    'data'=>$usuario,
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Usuario creado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al guardar usuario'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Usuario no creado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $usuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuario $usuario)
    {
        try{
            $mi_usuario=$request['usuario'];
            $usuario->nombre=$mi_usuario['nombre'];
            $usuario->apellidos=$mi_usuario['apellidos'];
            $usuario->fecha_nacimiento=$mi_usuario['fecha_nacimiento'];
            $usuario->email=$mi_usuario['email'];
            if($usuario->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Usuario actualizado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al actualizar usuario'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Usuario no actualizado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $usuario)
    {
        try{
            $usuario->user_estado='INACTIVO';
            if($usuario->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Usuario eliminado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al eliminar Usuario'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Usuario no eliminado'
             );
         }
         return response()->json($data,200);
    }

    public function confirmarEmail($user_id){
        $user=Usuario::find($user_id);
        if(!$user->email_verified_at){
            $user->email_verified_at=Carbon::now();
            $user->save();
            return \Redirect::to(env('APP_URL_FRONT').'#/auth/login');
        }else{
            return 'Su correo ya fue verificado anteriormente';
        }
    }

    // export XLS 
    public function exportXLS(){
        return \Excel::download(new UsuariosExport(),'usuarios.xlsx');
    }

     // export PDF 
     public function exportPDF(){
        $usuarios=Usuario::all();
        $pdf = \PDF::loadView('pdf.usuarios',compact('usuarios'));
        return $pdf->stream('usuarios.pdf');
    }
}
