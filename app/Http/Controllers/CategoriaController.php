<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Subcategoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categorias=Categoria::with('subcategorias')->where('cate_estado','ACTIVO')->orderBy('nombre_categoria','asc')->get();
            $data=array(
                'data'=>$categorias,
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Datos obtenidos'
            );
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Datos no encontrados'
            );
        }
        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
           $categoria=new Categoria($request['categoria']);
           if($categoria->save()){
                    $data=array(
                        'data'=>$categoria,
                        'status'=> 'success',
                        'code'=> 200,
                        'message'=> 'Categoría creada'
                    );
            }else{
                $data=array(
                    'status'=> 'error',
                    'code'=> 401,
                    'message'=> 'Error al guardar categoría'
                );
            }
       } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Categoría no creada'
            );
        }
        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        try{
            $mi_categoria=$request['categoria'];
            $categoria->nombre_categoria=$mi_categoria['nombre_categoria'];
            $categoria->codigo_categoria=$mi_categoria['codigo_categoria'];
            $categoria->descripcion_categoria=$mi_categoria['descripcion_categoria'];
            if($categoria->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Categoría actualizada'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al actualizar categoría'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Categoría no actualizada'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
        try{
            $categoria->cate_estado='INACTIVO';
            if($categoria->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Categoría eliminada'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al eliminar categoría'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Categoría no eliminada'
             );
         }
         return response()->json($data,200);
    }

    // actualizacion y creación de subcategorías 
    public function updateSubcategorias(Request $request,Categoria $categoria){
        try{
            Subcategoria::where('cate_id',$categoria->cate_id)->update(['sub_estado'=>'INACTIVO']);
            foreach($request['categoria']['subcategorias'] as $subcategoria){
                if(isset($subcategoria['sub_id'])){
                    $mi_subcategoria=Subcategoria::find($subcategoria['sub_id']);
                    $mi_subcategoria->sub_estado='ACTIVO';
                    $mi_subcategoria->sub_nombre=$subcategoria['sub_nombre'];
                    $mi_subcategoria->sub_codigo=$subcategoria['sub_codigo'];
                    $mi_subcategoria->sub_descripcion=$subcategoria['sub_descripcion'];
                    $mi_subcategoria->save();
                }else{
                    $subcategoria['cate_id']=$categoria->cate_id;
                    $new_subcategoria=new Subcategoria($subcategoria);
                    $new_subcategoria->save();
                }
            }
            $data=array(
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Subcategorías actualizadas'
            );
        }catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Error, Subcategorías no actualizadas'
            );
        }
        return response()->json($data,200);
    }
}
