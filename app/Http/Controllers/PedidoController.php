<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $jwtAuth= new JwtAuth();
            $user_id=$jwtAuth->getUserId($request->header('Authorization'));
            $pedidos=Pedido::with('producto')->where(['user_id'=>$user_id,'pedi_estado'=>'ACTIVO'])->get();
            $data=array(
                'data'=>$pedidos,
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Datos obtenidos'
            );
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Datos no encontrados'
            );
        }
        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $jwtAuth= new JwtAuth();
            $user_id=$jwtAuth->getUserId($request->header('Authorization'));
            $pedido=new Pedido($request['pedido']);
            $pedido->user_id=$user_id;
            if($pedido->save()){
                $data=array(
                    'data'=>$pedido,
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Pedido creado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al guardar pedido'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Pedido no creado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function show(Pedido $pedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function edit(Pedido $pedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pedido $pedido)
    {
        try{
            $pedido->fecha_pedido=$request['pedido']['fecha_pedido'];
            $pedido->cantidad=$request['pedido']['cantidad'];
            $pedido->prod_id=$request['pedido']['prod_id'];
            if($pedido->save()){
                $data=array(
                    'data'=>$pedido,
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Pedido actualizado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al guardar Pedido'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Pedido no creado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedido $pedido)
    {
        //
    }
}
