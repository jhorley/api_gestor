<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Foto;
use App\Models\Tarifa;
use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Exports\ProductosExport;
use App\Models\CategoriasProducto;
use Illuminate\Support\Facades\Storage;


class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $productos=Producto::with('categorias','fotos','tarifas')->where('prod_estado','ACTIVO')->orderBy('nombre_producto','asc')->get();
            $data=array(
                'data'=>$productos,
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Datos obtenidos'
            );
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Datos no encontrados'
            );
        }
        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $producto=new Producto($request['producto']);
            if($producto->save()){
                foreach ($request['categorias'] as  $cate_id) {
                    $categoria=Categoria::find($cate_id);
                    $categoria_productos=new CategoriasProducto();
                    $categoria_productos->cate_id=$cate_id;
                    $categoria_productos->prod_id=$producto->prod_id;
                    $hijas_ids=[];
                    // debemos conocer a cuál categoría pertenecen las subcategorías 
                    foreach ($categoria->subcategorias as $subcategoria) {
                        if(in_array($subcategoria->sub_id,$request['subcategorias'])){
                            array_push($hijas_ids,$subcategoria->sub_id);
                        }
                    }
                    $categoria_productos->hijas_ids=implode(',',$hijas_ids);
                    $categoria_productos->save();
                }
                $data=array(
                    'data'=>$producto,
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Producto creado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al guardar Producto'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Producto no creado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($fecha)
    {
        $mi_fecha=Carbon::create($fecha);
        $productos=Producto::with('tarifas')->where('prod_estado','ACTIVO')->get();
        $data=[];
        foreach ($productos as $key => $producto) {
            $precio=$producto->precio_fijo;
            foreach ($producto->tarifas as $j => $tarifa) {
                if($mi_fecha>=Carbon::create($tarifa['fecha_inicio']) && $mi_fecha<=Carbon::create($tarifa['fecha_fin'])){
                    $precio=$tarifa['precio'];
                }
            }
            $mi_producto=array('nombre_producto'=>$producto->nombre_producto,'precio'=>$precio);
            array_push($data,$mi_producto);
        }
        return response()->json($data,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        try{
            $mi_producto=$request['producto'];
            $producto->nombre_producto=$mi_producto['nombre_producto'];
            $producto->codigo_producto=$mi_producto['codigo_producto'];
            $producto->descripcion_producto=$mi_producto['descripcion_producto'];
            if($producto->save()){
                CategoriasProducto::where('prod_id',$producto->prod_id)->update(['caprod_estado'=>'INACTIVO','hijas_ids'=>NULL]);
                foreach ($request['categorias'] as  $cate_id) {
                    $categoria=Categoria::find($cate_id);
                    $existe=CategoriasProducto::where(['prod_id'=>$producto->prod_id,'cate_id'=>$cate_id])->first();
                    if($existe){
                        $categoria_productos=$existe;
                        $categoria_productos->caprod_estado='ACTIVO';
                    }else{
                        $categoria_productos=new CategoriasProducto();
                        $categoria_productos->cate_id=$cate_id;
                        $categoria_productos->prod_id=$producto->prod_id;
                    }
                    $hijas_ids=[];
                     // debemos conocer a cuál categoría pertenecen las subcategorías 
                     foreach ($categoria->subcategorias as $subcategoria) {
                        if(in_array($subcategoria->sub_id,$request['subcategorias'])){
                            array_push($hijas_ids,$subcategoria->sub_id);
                        }
                    }
                    $categoria_productos->hijas_ids=implode(',',$hijas_ids);
                    $categoria_productos->save();
                }
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Producto actualizado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al actualizar producto'
                 );
             }
        } catch (\Throwable $th) {
             $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Producto no actualizado'
             );
         }
         return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        try{
            $producto->prod_estado='INACTIVO';
            if($producto->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Producto eliminado'
                );
             }else{
                 $data=array(
                     'status'=> 'error',
                     'code'=> 401,
                     'message'=> 'Error al eliminar producto'
                 );
             }
        } catch (\Throwable $th) {
            $data=array(
                 'data'=>$th,
                 'status'=> 'error',
                 'code'=> 400,
                 'message'=> 'Producto no eliminado'
            );
         }
         return response()->json($data,200);
    }

    public function uploadFotoProducto(Request $request, Producto $producto){
        try{
            if(is_file($request['foto'])){
                $path_foto=Storage::disk('local')->put('productos/'.$producto->prod_id, $request['foto']);
                $foto=new Foto();
                $foto->url=env('APP_URL_IMGS').$path_foto;
                $foto->prod_id=$producto->prod_id;
            }
            if($foto->save()){
                $data=array(
                    'status'=> 'success',
                    'code'=> 200,
                    'message'=> 'Foto guardada'
                );
            }else{
                $data=array(
                    'status'=> 'error',
                    'code'=> 401,
                    'message'=> 'Ocurrió un error al guardar foto'
                );
            }
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Error, Foto no guardada'
            );
        }
        return response()->json($data,200);
    }

    public function saveTarifas(Request $request, Producto $producto){
        try{
            Tarifa::where('prod_id',$producto->prod_id)->update(['tari_estado'=>'INACTIVO']);
            foreach($request['tarifas'] as $tarifa){
                if(isset($tarifa['tari_id'])){
                    $modelo=Tarifa::find($tarifa['tari_id']);
                }else{
                    $modelo=new Tarifa();
                }
                $modelo->prod_id=$producto->prod_id;
                $modelo->fecha_inicio=$tarifa['fecha_inicio'];
                $modelo->fecha_fin=$tarifa['fecha_fin'];
                $modelo->precio=$tarifa['precio'];
                $modelo->tari_estado='ACTIVO';
                $modelo->save();
            }
            $data=array(
                'status'=> 'success',
                'code'=> 200,
                'message'=> 'Tarifas guardadas'
            );
        } catch (\Throwable $th) {
            $data=array(
                'data'=>$th,
                'status'=> 'error',
                'code'=> 400,
                'message'=> 'Error, Tarifas no guardadas'
            );
        }
        return response()->json($data,200);
    }

    // export XLS 
    public function exportXLS(){
        return \Excel::download(new ProductosExport(),'productos.xlsx');
    }

     // export PDF 
     public function exportPDF(Producto $producto){
        $categorias=$producto->categorias;
        $tarifas=$producto->tarifas;
        $pdf = \PDF::loadView('pdf.producto',compact('categorias','producto','tarifas'));
        return $pdf->stream('producto'.$producto->nombre_producto.'.pdf');
    }
}
