<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;

class JwtAuth{
    //mi key
    public $key;

    public function __construct(){
        $this->key='secret-key-gestor-89333777222883';
    }

    public function signup($email,$password,$getToken=null){
        $user=Usuario::where(['email'=>$email,'password'=>$password])->first();
        $signup=false;
        if(is_object($user)){
            // generar token
            $token=array(
                'user_id'=>$user->user_id,
                'email'=>$user->email,
                'nombre'=>$user->nombre,
                'apellidos'=>$user->apellidos,
                'iat'=>time(),
                'exp'=>time()+(2*24*60*60)
            );
           
            $jwt=JWT::encode($token, $this->key, 'HS256');
            $decoded=JWT::decode($jwt, $this->key, array('HS256'));

            if(is_null($getToken)){
                $data=array('user'=>$user,'token'=>$jwt);
                return array('data'=> $data,'status'=>'success');
            }else{
                $data=array('user'=>$user,'token'=>$decoded);
                return array('data'=>$data,'status'=>'success');
            }
        }else{
            $user=Usuario::where(['email'=>$email])->first();
            if(is_object($user)){
                return array('status'=>'error','message'=>'Password incorrecto');
            }else{
                return array('status'=>'error','message'=>'Usuario no existe');
            }
        }
    }

    public function checkToken($jwt,$getIdentity=false){
        $auth=false;
        try{
            $decoded=JWT::decode($jwt,$this->key,array('HS256'));
        }catch(\UnexpectedValueException $e){
            $auth=false;
        }
        catch(\DomainException $e){
            $auth=false;
        }

        if(isset($decoded) && is_object($decoded)){
            $auth=true;
        }else{
            $auth=false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;
    }

    public function getUserId($jwt){
        try {
            $decoded=JWT::decode($jwt, $this->key, array('HS256'));
            if(is_object($decoded)){
                return $decoded->user_id;
            }else{
                return array('status'=>'error','code'=>404,'message'=>'No se encontró Usuario con ese token');
            }
        } catch (\Throwable $th) {
            return array('status'=>'error','code'=>400,'message'=>'Token mal formado');
        }
    }
}