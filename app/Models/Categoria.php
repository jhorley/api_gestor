<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $primaryKey = "cate_id";

    protected $fillable = [
        'nombre_categoria',
        'codigo_categoria',
        'descripcion_categoria',
        'cate_estado',
        'created_at',
        'updated_at', 
    ];

    public function subcategorias(){
        return $this->hasMany('App\Models\Subcategoria','cate_id')->where('sub_estado','ACTIVO');
    }
}
