<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $primaryKey = "user_id";

    protected $fillable = [
        'nombre',
        'apellidos',
        'fecha_nacimiento',
        'email',
        'password',
        'foto_perfil',
        'email_verified_at',
        'user_estado',
        'created_at',
        'updated_at', 
    ];

    protected $hidden = [
        'password'
    ];
}
