<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $primaryKey = "prod_id";

    protected $fillable = [
        'nombre_producto',
        'codigo_producto',
        'descripcion_producto',
        'precio_fijo',
        'prod_estado',
        'created_at',
        'updated_at', 
    ];

    public function categorias(){
        return $this->belongsToMany('App\Models\Categoria','categorias_productos','prod_id','cate_id')
            ->with('subcategorias')
            ->withPivot('caprod_estado','hijas_ids')
            ->where('caprod_estado','ACTIVO');
    }

    public function fotos(){
        return $this->hasMany('App\Models\Foto','prod_id')->where('foto_estado','ACTIVO');
    }

    public function tarifas(){
        return $this->hasMany('App\Models\Tarifa','prod_id')->where('tari_estado','ACTIVO');
    }
}
