<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    use HasFactory;

    protected $primaryKey = "sub_id";

    protected $fillable = [
        'sub_nombre',
        'sub_codigo',
        'sub_descripcion',
        'cate_id',
        'sub_estado',
        'created_at',
        'updated_at', 
    ];
}
