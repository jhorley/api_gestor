<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $primaryKey = "pedi_id";

    protected $fillable = [
        'fecha_pedido',
        'prod_id',
        'cantidad',
        'user_id',
        'pedi_estado',
        'created_at',
        'updated_at', 
    ];

    public function producto(){
        return $this->belongsTo('App\Models\Producto','prod_id');
    }
    
}
