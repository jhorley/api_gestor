<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriasProducto extends Model
{
    use HasFactory;

    protected $primaryKey = "caprod_id";

    protected $fillable = [
        'cate_id',
        'prod_id',
        'hijas_ids',
        'caprod_estado',
        'created_at',
        'updated_at', 
    ];
}
