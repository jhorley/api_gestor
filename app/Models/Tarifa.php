<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{
    use HasFactory;

    protected $primaryKey = "tari_id";

    protected $fillable = [
        'fecha_inicio',
        'fecha_fin',
        'precio',
        'prod_if',
        'tari_estado',
        'created_at',
        'updated_at', 
    ];

}
