<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\TokenIsValid;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// usuarios
Route::post('login','App\Http\Controllers\UsuarioController@login');
Route::post('registrar','App\Http\Controllers\UsuarioController@store');
Route::resource('usuarios','App\Http\Controllers\UsuarioController')->middleware(TokenIsValid::class);
Route::get('sendEmail/{user_id}','App\Http\Controllers\MailController@sendConfirmation');
Route::get('usuarios/confirmar/{user_id}','App\Http\Controllers\UsuarioController@confirmarEmail');
Route::get('getUsuario','App\Http\Controllers\UsuarioController@getUsuario');
Route::get('usuariosXLS','App\Http\Controllers\UsuarioController@exportXLS');
Route::get('usuariosPDF','App\Http\Controllers\UsuarioController@exportPDF');

//categorias
Route::resource('categorias','App\Http\Controllers\CategoriaController')->middleware(TokenIsValid::class);
Route::post('saveSubcategorias/{categoria}','App\Http\Controllers\CategoriaController@updateSubcategorias');

//productos
Route::resource('productos','App\Http\Controllers\ProductoController')->middleware(TokenIsValid::class);
Route::get('productos/{fecha}','App\Http\Controllers\ProductoController@show')->middleware(TokenIsValid::class);
Route::post('uploadFotoProducto/{producto}','App\Http\Controllers\ProductoController@uploadFotoProducto');
Route::post('saveTarifas/{producto}','App\Http\Controllers\ProductoController@saveTarifas');
Route::get('productosXLS','App\Http\Controllers\ProductoController@exportXLS');
Route::get('productoPDF/{producto}','App\Http\Controllers\ProductoController@exportPDF');

// pedidos 
Route::resource('pedidos','App\Http\Controllers\PedidoController');


