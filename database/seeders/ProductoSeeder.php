<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\CategoriasProducto;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Borramos los datos de la tabla
       \DB::table('productos')->delete();
       \DB::table('categorias_productos')->delete();
       // Añadimos una entrada a esta tabla
       Producto::create(array(
           'nombre_producto'=>'Balón',
           'codigo_producto'=>'111109',
           'descripcion_producto' => 'Descripción prueba',
           'precio_fijo'=>'20'
       ));

        Producto::create(array(
            'nombre_producto'=>'Bicicleta',
            'codigo_producto'=>'200292',
            'descripcion_producto' => 'Descripción bici',
            'precio_fijo'=>'80'
        ));

        // agregamos las categorías
        CategoriasProducto::create(array('prod_id'=>1,'cate_id' => 1));
        CategoriasProducto::create(array('prod_id'=>2,'cate_id' => 2));

    }
}
