<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Borramos los datos de la tabla
       \DB::table('usuarios')->delete();
        // Añadimos una entrada a esta tabla
        Usuario::create(array(
            'nombre'=>'Jhorley',
            'apellidos'=>'Ceballos Vanegas',
            'email' => 'jhorleyandres@gmail.com',
            'fecha_nacimiento' => '1992-11-29',
            'email_verified_at' => '2021-03-30',
            'password'=>'8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'
        ));
    }
}
