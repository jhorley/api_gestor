<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategorias', function (Blueprint $table) {
            $table->increments('sub_id');
            $table->string('sub_nombre');
            $table->string('sub_codigo');
            $table->mediumText('sub_descripcion')->nullable();
            $table->integer('cate_id')->unsigned()->nullable();
            $table->foreign('cate_id')->references('cate_id')->on('categorias');
            $table->enum('sub_estado', array('ACTIVO','INACTIVO'))->default('ACTIVO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategorias');
    }
}
