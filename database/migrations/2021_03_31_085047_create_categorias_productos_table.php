<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias_productos', function (Blueprint $table) {
            $table->increments('caprod_id');
            $table->integer('cate_id')->unsigned()->nullable();
            $table->foreign('cate_id')->references('cate_id')->on('categorias');
            $table->integer('prod_id')->unsigned()->nullable();
            $table->foreign('prod_id')->references('prod_id')->on('productos');
            $table->mediumText('hijas_ids')->nullable();
            $table->enum('caprod_estado', array('ACTIVO','INACTIVO'))->default('ACTIVO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias_productos');
    }
}
