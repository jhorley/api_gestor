<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Producto</title>
     <style>
       table {
            width: 100%;
        }
        th{
            text-align:center;
            background-color:#3490dc;
            color:white;
        }
        td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <hr>
    <br>
    <h2>Detalles Producto</h2>
    <table >
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Código</th>
                <th>Precio fijo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$producto['nombre_producto']}}</td>
                <td>{{$producto['codigo_producto']}}</td>
                <td>{{$producto['precio_fijo']}}</td>
            </tr>
        </tbody>
    </table>

    <h3>Categorías</h3>
    <table >
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Código</th>
                <th>Descripción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categorias as $categoria)
            <tr>
                <td>{{$categoria['nombre_categoria']}}</td>
                <td>{{$categoria['codigo_categoria']}}</td>
                <td>{{$categoria['descripcion_categoria']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <h3>Tarifas</h3>
    <table >
        <thead>
            <tr>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Precio</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tarifas as $tarifa)
            <tr>
                <td>{{$tarifa['fecha_inicio']}}</td>
                <td>{{$tarifa['fecha_fin']}}</td>
                <td>{{$tarifa['precio']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>