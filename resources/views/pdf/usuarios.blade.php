<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuarios</title>
     <style>
       table {
            width: 100%;
        }
        th{
            text-align:center;
            background-color:#3490dc;
            color:white;
        }
        td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <hr>
    <br>
    <table >
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Email</th>
                <th>Nacimiento</th>
                <th>Email verificado</th>
            </tr>
        </thead>
        <tbody>
           @foreach($usuarios as $usuario)
                <tr>
                    <td>{{$usuario['nombre']}}</td>
                    <td>{{$usuario['apellidos']}}</td>
                    <td>{{$usuario['email']}}</td>
                    <td>{{$usuario['fecha_nacimiento']}}</td>
                    <td>
                        @if($usuario['fecha_nacimiento'])
                            SI
                        @else
                            NO
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>